<?php

/**
 * @file
 * Defines the single shipment splitter.
 */

$plugin = array(
  'title' => t('Single shipment'),
  'description' => t('Group my order into a single shipment once all products are available.'),
  'weight' => -10,
);

/**
 * Callback to evaluate the single shipment splitting.
 *
 * This always returns one shipment.
 *
 * @param object $order
 *   The commerce order.
 *
 * @return bool
 *   TRUE if the splitter plugin applies.
 */
function commerce_shipment_shipment_splitting_single_applies($order) {
  return TRUE;
}

function commerce_shipment_shipment_splitting_single_process($order) {
  /** @var EntityDrupalWrapper $wrapper */
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $shipment = entity_create('commerce_shipment', array(
    'order_id' => $wrapper->getIdentifier(),
    // @todo should the bundle be configurable on checkout pane.
    'type' => 'shipment',
  ));
  /** @var EntityDrupalWrapper $shipment_wrapper */
  $shipment_wrapper = entity_metadata_wrapper('commerce_shipment', $shipment);
  /** @var EntityDrupalWrapper $line_item_wrapper */
  foreach ($wrapper->get('commerce_line_items') as $line_item_wrapper) {
    $shipment_wrapper->commerce_shipment_line_items[] = $line_item_wrapper->getIdentifier();
  }
  entity_save('commerce_shipment', $shipment);
  $wrapper->commerce_shipments[] = $shipment;
  $wrapper->save();
}
