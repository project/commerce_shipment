<?php

/**
 * @file
 * Commerce Shipment hooks for Drupal Commerce.
 */

/**
 * Implements hook_commerce_checkout_pane_info().
 */
function commerce_shipment_commerce_checkout_pane_info() {
  $checkout_panes = array();

  if (module_exists('commerce_shipping')) {
    $checkout_panes['commerce_shipment_split_shipments'] = array(
      'title' => variable_get('commerce_shipment_split_shipments_pane_title', t('Split shipment options')),
      'name' => t('Split shipments'),
      'file' => 'includes/commerce_shipment.checkout_pane.inc',
      'page' => 'shipping',
      'locked' => FALSE,
      'weight' => -10,
    );
  }

  return $checkout_panes;
}
