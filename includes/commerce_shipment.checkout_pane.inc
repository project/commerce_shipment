<?php

/**
 * @file
 * Checkout pane callback functions for the shipment auto module.
 */

/**
 * Checkout pane callback: returns the customer profile pane's settings form.
 */
function commerce_shipment_split_shipments_settings_form($checkout_pane) {
  $form = array();

  $form['commerce_shipment_split_shipments_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable shipment splitting'),
    '#default_value' => variable_get('commerce_shipment_split_shipments_enabled', FALSE),
  );

  $form['container'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'input[name="commerce_shipment_split_shipments_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['container']['commerce_shipment_split_shipments_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display title'),
    '#description' => t('The display title for the checkout pane during customer checkout.'),
    '#default_value' => variable_get('commerce_shipment_split_shipments_pane_title', t('Split shipment options')),
  );
  $form['container']['commerce_shipment_split_shipments_shipment_type'] = array(
    '#type' => 'select',
    '#title' => t('Shipment type'),
    '#options' => array(),
    '#default_value' => variable_get('commerce_shipment_split_shipments_shipment_type', 'shipment'),
  );
  foreach (commerce_shipment_types() as $shipment_type) {
    $form['container']['commerce_shipment_split_shipments_shipment_type']['#options'][$shipment_type['type']] = $shipment_type['name'];
  }
  if (count($form['container']['commerce_shipment_split_shipments_shipment_type']['#options']) == 1) {
    $form['container']['commerce_shipment_split_shipments_shipment_type']['#disabled'] = TRUE;
  }
  $form['container']['commerce_shipment_split_shipments_plugins'] = array(
    '#type' => 'tableselect',
    '#tree' => TRUE,
    '#header' => array(
      'plugin' => t('Splitting method'),
    ),
    '#multiple' => TRUE,
    '#options' => array(),
    '#default_value' => variable_get('commerce_shipment_split_shipments_plugins', array()),
  );

  $plugins = commerce_shipment_shipment_splitting_plugins();
  foreach ($plugins as $plugin_id => $plugin_definition) {
    $form['container']['commerce_shipment_split_shipments_plugins']['#options'][$plugin_id] = array(
      'title' => array(
        'data' => array('#title' => $plugin_definition['title']),
      ),
      'plugin' => t('<strong>@title</strong><br />@description', array(
        '@title' => $plugin_definition['title'],
        '@description' => $plugin_definition['description'],
      )),
    );
  }
  return $form;
}

/**
 * Checkout pane callback: returns a customer profile edit form.
 */
function commerce_shipment_split_shipments_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Do not attempt to split shipment unless enabled.
  if (!variable_get('commerce_shipment_split_shipments_enabled', FALSE)) {
    return NULL;
  }

  // Make sure the order is in fact shippable.
  if (!commerce_physical_order_shippable($order)) {
    return NULL;
  }

  $shipment_splitter_plugins = commerce_shipment_shipment_splitting_plugins();
  $enabled_shipment_splitters = variable_get('commerce_shipment_split_shipments_plugins', array());
  $available_shipment_splitters = array();

  foreach ($enabled_shipment_splitters as $plugin_id) {
    $plugin = $shipment_splitter_plugins[$plugin_id];
    if (is_callable($plugin['applies']) && call_user_func($plugin['applies'], $order)) {
      $available_shipment_splitters[$plugin_id] = $plugin['description'];
    }
  }
  $order->data['commerce_shipment']['available_shipment_splitters'] = $available_shipment_splitters;

  // Get the current splitting plugin from form state values, or the default
  // from the available options.
  $pane_values = !empty($form_state['values'][$checkout_pane['pane_id']]) ? $form_state['values'][$checkout_pane['pane_id']] : array();
  if (isset($pane_values['shipment_splitter_options']) && !empty($pane_values['shipment_splitter_options'])) {
    $current_splitting_plugin = $form_state['values'][$checkout_pane['pane_id']]['shipment_splitter_options'];
  }
  elseif (isset($order->data['commerce_shipment']['shipment_splitter'])) {
    $current_splitting_plugin = $order->data['commerce_shipment']['shipment_splitter'];
  }
  else {
    $current_splitting_plugin = key($available_shipment_splitters);
  }

  // Check if the order has a specified splitter plugin, yet.
  if (isset($order->data['commerce_shipment']['shipment_splitter'])) {
    // Check if the specified plugin is different than the one determined
    // previously.
    if ($order->data['commerce_shipment']['shipment_splitter'] != $current_splitting_plugin) {
      // If they do not match, we need to run the splitter plugin, and we need
      // to set the plugin in the order.
      $order->data['commerce_shipment']['shipment_splitter'] = $current_splitting_plugin;
      commerce_shipment_process_shipment_splitting_plugin($current_splitting_plugin, $order);
    }
  }
  else {
    // Set the default splitter and run it.
    $order->data['commerce_shipment']['shipment_splitter'] = $current_splitting_plugin;
    commerce_shipment_process_shipment_splitting_plugin($current_splitting_plugin, $order);
  }

  // If there is only one available shipment splitter, use it and do not show
  // the form to the end user.
  if (count($available_shipment_splitters) == 1) {
    return NULL;
  }

  $pane_form = array(
    '#parents' => array($checkout_pane['pane_id']),
    '#prefix' => '<div id="commerce-shipment-split-shipments-ajax-wrapper">',
    '#suffix' => '</div>',
  );
  $pane_form['shipment_splitter_options'] = array(
    '#type' => 'radios',
    '#title' => variable_get('commerce_shipment_split_shipments_pane_title', t('Split shipment options')),
    '#title_display' => 'invisible',
    '#options' => $available_shipment_splitters,
    '#default_value' => $current_splitting_plugin,
    '#ajax' => array(
      'callback' => 'commerce_shipment_split_shipments_refresh',
      'wrapper' => 'commerce-shipment-split-shipments-ajax-wrapper',
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'commerce_shipping') . '/js/commerce_shipping.js',
        drupal_get_path('module', 'commerce_shipment') . '/js/commerce-shipment-splitting-pane.js',
      ),
    ),
  );

  return $pane_form;
}

/**
 * Ajax callback: Returns recalculated shipping services.
 */
function commerce_shipment_split_shipments_refresh($form, $form_state) {
  $commands = array();
  $commands[] = ajax_command_insert('#commerce-shipment-split-shipments-ajax-wrapper', render($form['commerce_shipment_split_shipments']));
  $commands[] = array(
    'command' => 'commerceCheckShippingRecalculation',
  );
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Checkout pane callback: submits a customer profile edit form.
 */
function commerce_shipment_split_shipments_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $pane_id = $checkout_pane['pane_id'];
  $values = $form_state['values'][$pane_id];
  $splitting_plugin = $values['shipment_splitter_options'];

  // If we have a new splitting plugin, process it.
  if ($splitting_plugin != $order->data['commerce_shipment']['shipment_splitter']) {
    commerce_shipment_process_shipment_splitting_plugin($splitting_plugin, $order);
  }
}

/**
 * Checkout pane callback: returns the cart contents review data for the
 *   Review checkout pane.
 */
function commerce_shipment_split_shipments_pane_review($form, $form_state, $checkout_pane, $order) {
  // @todo Implement review.
}

