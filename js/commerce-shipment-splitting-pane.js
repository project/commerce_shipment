(function ($, Drupal) {
  // @todo this should live in commerce_shipping
  Drupal.ajax.prototype.commands.commerceCheckShippingRecalculation = function (ajax, response, status) {
    return $.fn.commerceCheckShippingRecalculation();
  }
})(jQuery, Drupal);
